# 39IOS Det 1 Docker Definitions

This repo containers to dockerfiles necessary to build images required for public pipelines.

## Quick Start

1. Create a branch and MR for your new/modified dockerfile
2. Create a folder named the same as the image you want to define
3. Create a Dockerfile in the folder. Include any extra files the image needs inside this folder, as well
4. Create a job in the deploy stage similar to below
```yml
generic-cyt:
  image: docker:latest
  variables:
    FULL_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME
  stage: deploy
  services:
    - docker:dind
  script:
    - docker build --pull -t $FULL_NAME .
    - docker tag $FULL_NAME:latest $FULL_NAME:$CI_COMMIT_SHORT_SHA
    - docker push $FULL_NAME
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
      - generic-cyt/*
```
   - Note: If you would like this to create the image on a different repo's registry, redefine the $CI_REGISTRY_IMAGE environment variable to your repos registry.
   - Note: In rules.changes you must specify the path of the Dockerfile directory. $CI_JOB_NAME does not seem to work as this time

5. Add yourself to the CODEOWNERS file to take ownership of your image definition
```sh
generic-cyt/* @my-username
```

## Workflow
This repo uses Gitlab's [CODEOWNERS](https://docs.gitlab.com/ee/user/project/code_owners.html) feature. The idea is you will need maintainer approval when you create a new image (which will include taking ownership of your image folder via the CODEOWNERS file). Once your image definition is merge to master you will be able to merge the image folders you own directly to master without approval so you can redefine your image as your project evolves. If the pipeline fails, don't worry, the pipeline is designed to only run image build jobs on images that change not otherwise. You will need to fix your dockerfile and push back to master. Building on your local should happen first to verify that the image will build properly before merging to master.


## Pipeline Environment Variables

There are many gitlab runner environment variables that are built into every runner (depending on the version). They are located [here](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
